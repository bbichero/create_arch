#!/bin/bash

# TODO: handle multiple ip / like /32 /24 /16
# Check if library exist in current path

library_path=".."

if [ -f "${library_path}/shell_library/type_functions.sh" ]
then
    # Include library
    . ${library_path}/shell_library/type_functions.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# ARG
OPTS=`getopt -o :s:d:i:f: --long source:,destination:,ip: -n 'parse-options' -- "$@"`

#if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

# Default option value
DEBUG=1
port_src=""
port_dst=""
ip_remote=""
flush=""

# If no argument print usage line
if [ $1 == "--" ]
then
	echo -e "${RED}Error: usage ./ft_forward_ports.sh -s <source-port-on-vm> -d <destination-port-one-gate> -i <vm-ip> ${NC}">&2 
	exit 1
fi

# Check arguments
while true; do
	case "$1" in
		-s|--source)
			port_src=$2
			if ! ft_is_int "$port_src"
			then
				exit 1;
			fi

			shift 2 ;;
		-d|--destination)
			port_dst=$2
			if ! ft_is_int "$port_dst"
			then
				exit 1;
			fi

			shift 2 ;;
		-i|--ip)
			ip_remote=$2
			if ! ft_is_ipv4 "$ip_remote"
			then
				exit 1;
			fi

			shift 2 ;;
		-f|--flush)
			flush=$2
			# If flush command was run with 'yes' argument,
			if [ "$flush" != 'yes' ]
			then
				echo -e ${RED}"usage of flush: ./ft_forward_ports -f 'yes'"${NC}
				exit 1
			fi

			# check if fw.stop file exist and execute it
			if [ -f "fw.stop" ]
			then
				# Chekc if file have execution aceess
				if [ -x "fw.stop" ]
				then
			 		echo -e ${YEL}"Flush all iptables rules"${NC}
					./fw.stop
				else
					echo -e ${RED}"Can't execute file 'fw.stop' no execution permission, abort"${NC}
				fi
			else
				echo -e ${RED}"Can't find file 'fw.stop', abort"${NC}
			fi
			shift 2 ;;
		\?)
    			echo "Invalid option: -$OPTARG" >&2 ; exit 1 ;;
  		:)
    			echo "Option -$OPTARG requires an argument." >&2 ; exit 1 ;;
		--) shift ; break ;;
		*)
			echo -e "${RED}Error: usage ./ft_forward_ports.sh -s <source-port-on-vm> -d <destination-port-one-gate> -i <vm-ip> ${NC}">&2 ; exit 1 ;;
	esac
done

# Check if mandatory argument are provided non null
if [ ${#port_src} -eq 0 ]
then
	echo -e ${RED}"is, --source argument is not provided, abort"${NC}
	exit 1
fi

if [ ${#port_dst} -eq 0 ]
then
	echo -e ${RED}"-d, --destination argument is not provided, abort"${NC}
	exit 1
fi

if [ ${#ip_remote} -eq 0 ]
then
	echo -e ${RED}"-i, --ip argument is not provided, abort"${NC}
	exit 1
fi


# Check if /etc/iptables.up.rules file exist
if [ ! -f "/etc/iptables.up.rules" ]
then
	echo -e ${RED}"Can't fin file '/etc/iptables.up.rules', please run"
	echo -e "./ft_ini_gateway.sh file first and re-run this file"${NC}
	exit 1
fi

# Check if /etc/network/ip-pre-up.d/iptables file exist
if [ ! -f "/etc/network/if-pre-up.d/iptables" ]
then
	echo -e ${RED}"Can't find file '/etc/network/if-pre-up.d/iptables', please run "
	echo -e "./ft_ini_gateway.sh file and re-run this file"${NC}
	exit 1
fi

# Grep on /etc/iptables.up.rules to check if port not already used
# Grep for prerouting, postrouting, and forwarding
if [ $DEBUG -gt 0 ]
then
	echo "grep -oP '\-\-dport( +)$port_dst(.*)--to-destination( +)${ip_remote}:${port_src}' /etc/iptables.up.rules"
fi

# Prerouting grep
grep_preroute=$(grep -noP "\-\-dport( +)$port_dst(.*)--to-destination( +)${ip_remote}:${port_src}" /etc/iptables.up.rules  | cut -f1 -d:)

# If a prerouting rule is already set for this port and ip abort
if [ "${grep_preroute}" != "" ]
then
	echo -e ${RED}"A PREROUTING rule is already set in /etc/iptables.up.rules:"${NC}
	echo -n "line number ${grep_preroute}:  "
	sed -n "${grep_preroute}p" /etc/iptables.up.rules
else
	# Add iptables prerouting rule
	echo -e ${YEL}"iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport ${port_dst} -j DNAT --to-destination ${ip_remote}:${port_src}"${NC}
	iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport ${port_dst} -j DNAT --to-destination ${ip_remote}:${port_src}
fi

# If DEBUG>0 print info
if [ $DEBUG -gt 0 ]
then
	echo "grep -oP '\-\-dport( +)$port_dst(.*)--to-destination( +)${ip_remote}:${port_src}' /etc/iptables.up.rules"
fi

# Postrouting grep
grep_postroute=$(grep -noP "${ip_remote}(.*)\-\-dport( +)${port_src}( +)-j MASQUERADE" /etc/iptables.up.rules  | cut -f1 -d:)

# If a postrouting rule is already set for this port and ip abort
if [ "${grep_postroute}" != "" ]
then
	echo -e ${RED}"A POSTROUTING rule is already set in /etc/iptables.up.rules:"${NC}
	echo -n "line number ${grep_postroute}:  "
	sed -n "${grep_postroute}p" /etc/iptables.up.rules
else
	# Add iptables postrouting rule
	echo -e ${YEL}"iptables -t nat -A POSTROUTING -d ${ip_remote}/32 -p tcp -m tcp --dport ${port_src} -j MASQUERADE"${NC}
	iptables -t nat -A POSTROUTING -d ${ip_remote}/32 -p tcp -m tcp --dport ${port_src} -j MASQUERADE
fi

# If DEBUG>0 print info
if [ $DEBUG -gt 0 ]
then
	echo "grep -noP '${ip_remote}(.*)\-\-sport( +)${port_src}( +)-j ACCEPT' /etc/iptables.up.rules"
fi

# source forward grep
grep_src_forward=$(grep -noP "${ip_remote}(.*)\-\-sport( +)${port_src}( +)-j ACCEPT" /etc/iptables.up.rules  | cut -f1 -d:)

# If a src forward rule is already set for this port and ip abort
if [ "${grep_src_forward}" != "" ]
then
	echo -e ${RED}"A source FORWARD rule is already set in /etc/iptables.up.rules:"${NC}
	echo -n "line number ${grep_src_forward}:  "
	sed -n "${grep_src_forward}p" /etc/iptables.up.rules
else
	# Add iptables source forwardin rule
	echo -e ${YEL}"iptables -A FORWARD -s ${ip_remote}/32 -i eth0 -p tcp -m tcp --sport ${port_src} -j ACCEPT"${NC}
	iptables -A FORWARD -s ${ip_remote}/32 -i eth0 -p tcp -m tcp --sport ${port_src} -j ACCEPT	
fi

# If DEBUG>0 print info
if [ $DEBUG -gt 0 ]
then
	echo "grep -noP '${ip_remote}(.*)\-\-dport( +)${port_dst}( +)-j ACCEPT' /etc/iptables.up.rules"
fi

# destination forward grep
grep_dst_forward=$(grep -noP "${ip_remote}(.*)\-\-dport( +)${port_dst}( +)-j ACCEPT" /etc/iptables.up.rules  | cut -f1 -d:)

# If a dst forward rule is already set for this port and ip abort
if [ "${grep_dst_forward}" != "" ]
then
	echo -e ${RED}"A destination FORWARD rule is already set in /etc/iptables.up.rules:"${NC}
	echo -n "line number ${grep_dst_forward}:  "
	sed -n "${grep_dst_forward}p" /etc/iptables.up.rules
else
	# Add iptables source forwardin rule
	echo -e ${YEL}"iptables -A FORWARD -d ${ip_remote}/32 -i eth0 -p tcp -m tcp --dport ${port_dst} -j ACCEPT"${NC}
	iptables -A FORWARD -d ${ip_remote}/32 -i eth0 -p tcp -m tcp --dport ${port_dst} -j ACCEPT
fi

echo -e ${GRE}"Save iptables rule previously run"
echo -e "/etc/network/if-pre-up.d/iptables"${NC}
/etc/network/if-pre-up.d/iptables
