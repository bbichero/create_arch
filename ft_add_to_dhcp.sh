#!/bin/bash
# TODO: Mpre complete regex of dhcp_pattern
# TODO: script file for check validity of /etc/dhcp/dhcp.conf

library_path=".."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ]
then
    # Include library
    . ${library_path}/shell_library/type_functions.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Script file execute for add new entry in /etc/dhcp/dhcp.conf file
# of DHCP vm

# ARG
OPTS=`getopt -o :s:m:i:h:d: --long source-dhcp:,mac-addr:,ip:,hostname,domain-name: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

# Default option value
source_dhcp=""
mac_addr=""
ipv4=""
hostname=""
domain_name=""

# If no argument print usage line
if [ $1 == "--" ]
then
	echo "Error: usage ./ft_add_to_dhcp.sh -h <hostname>  -i <local-ip>"
	exit 1
fi

# Check arguments
while true; do
	case "$1" in
		-s|--source-dhcp)
			source_dhcp=$2	
			if ! ft_is_ipv4 "$source_dhcp"
			then
				exit 1;
			fi
			shift 2 ;;
		-i|--ip)
			ipv4=$2
			if ! ft_is_ipv4 "$ipv4"
			then
				exit 1;
			fi
			shift 2 ;;
		-d|--domain-name)
			domain_name=$2
			shift 2 ;;
		-h|--hostname)
			hostname=$2
			shift 2 ;;
		-m|--mac-addr)
			mac_addr=$2
			if ! ft_is_mac_addr "$mac_addr"
			then
				exit 1;
			fi
			shift 2 ;;
		\?)
    			echo "Invalid option: -$OPTARG" >&2 ; exit 1 ;;
  		:)
    			echo "Option -$OPTARG requires an argument." >&2 ; exit 1 ;;
		--) shift ; break ;;
		*)
			echo -e "${RED}Error: usage ./add_to_dhcp.sh -h <hostname>  -i <local-ip>${NC}">&2 ; exit 1 ;;
	esac
done

# Check all required value (by arguments)
if [ ${#source_dhcp} -eq 0 ]
then
	echo -e ${RED}"IPv4 of dhcp host not provided, abort"${NC}
	exit 1
fi

if [ ${#ipv4} -eq 0 ]
then
	echo -e ${RED}"IPv4 of new virtual machine not provided, abort"${NC}
	exit 1
fi

if [ ${#domain_name} -eq 0 ]
then
	echo -e ${RED}"Domain name of new virtual machine not provided, abort"${NC}
	exit 1
fi

if [ ${#hostname} -eq 0 ]
then
	echo -e ${RED}"Hostname of new virtual machine not provided, abort"${NC}
	exit 1
fi

if [ ${#mac_addr} -eq 0 ]
then
	echo -e ${RED}"MAC address of new virtual machine not provided, abort"${NC}
	exit 1
fi

# dhcp pattern entrie
dhcp_pattern="host ${hostname}.${domain_name}"

dhcp_entrie="\n# DHCP entrie for ${hostname}.${domain_name}
host ${hostname}.${domain_name} {
  hardware ethernet ${mac_addr};
  fixed-address ${ipv4};
}\n"

# Check if ip ping
echo -e "${YEL}ping_result=$(ping -c1 $source_dhcp > /dev/null && echo true || echo false)${NC}"
if [ "$(ping -c1 $source_dhcp > /dev/null && echo true || echo false)" != true ]
then
	echo ${RED}"DHCP host not reachable, abort"${NC}
	return 1
fi

# If DEBUG>0 print info
if [ ${DEBUG} -gt 0 ]
then
	echo -e ${YEL}"Check if ssh connection must can be done via rsa key"${NC}
fi

# Check if can connect with rsa key
echo -e ${YEL}"ssh -o PasswordAuthentication=no root@${source_dhcp} exit &>/dev/null"${NC}
ssh -o PasswordAuthentication=no root@${source_dhcp} exit &>/dev/null
if [ "$(test $? == 0 && echo 'true' || echo 'false')" == "false" ]
then
	echo -e ${RED}"Can't connection to DHCP host with rsa key,"
	echo -e "Please run ssh_rsa_connection.sh script to config ssh connection with rsa key"${NC}
	exit 1
fi

# If DEBUG>0 print info
if [ ${DEBUG} -gt 0 ]
then
	echo -e ${YEL}"Check entry in /etc/dhcp/dhcpd.conf file, with: "
	echo -e "${dhcp_pattern}"${NC}
fi

# Connection to dhcp host
echo "ssh root@${source_dhcp} 'grep -oP "$dhcp_pattern" /etc/dhcp/dhcpd.conf 2>&1'"
dhcp_entry_exist="$(ssh root@${source_dhcp} "grep -oP '$dhcp_pattern' /etc/dhcp/dhcpd.conf" 2>&1)"

# Check if dhcp entrie isn't already in /etc/dhcp/dhcp/conf file
if [ "${dhcp_entry_exist}" != "" ]
then
	echo -e ${RED}"A dhcp entries is in /etc/dhcp/dhcpd.conf file with:"
	echo -e "$dhcp_entry_exist"${NC}
	exit 1
else
	# Before make any change make a backup of /etc/dhcp/dhcpd.conf file
	echo -e ${YEL}"cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak"${NC}
	cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak	

	# Add to /etc/dhcp/dhcp.conf file a new entrie
	echo -e ${YEL}"ssh root@${source_dhcp} 'echo -e "${dhcp_entrie}"' >> /etc/dhcp/dhcpd.conf"${NC}
	ssh root@${source_dhcp} "echo -e '${dhcp_entrie}' >> /etc/dhcp/dhcpd.conf"
	echo -e ${GRE}"service isc-dhcp-server restart"${NC}

	# Restart DHCP service
	service isc-dhcp-server restart

	exit 0 
fi

# Make a backup
