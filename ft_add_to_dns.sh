#!/bin/bash

library_path=".."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ]
then
    # Include library
    . ${library_path}/shell_library/type_functions.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Add entry in :
# /etc/hosts
# /etc/bind9/db.192
# /etc/bind9/db.slash16.local

# ARG
OPTS=`getopt -o i:d:h: --long ip:,domain-name:,hostname: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

# Default option value
ipv4=""
hostname=""
domain_name=""

# If no argument print usage line
if [ $1 == "--" ]
then
	echo "Error: usage ./add_dns.sh -h <hostname>  -i <local-ip>"
	exit 1
fi

# Check arguments
while true; do
	case "$1" in
		-i|--ip)
			ipv4=$2
			if ft_is_ipv4 $ipv4
			then
				exit 1;
			fi

			shift 2 ;;
		-d|--domain-name)
			domain_name=$2
			shift 2 ;;
		-h|--hostname)
			hostname=$2
			shift 2 ;;
		\?)
    			echo "Invalid option: -$OPTARG" >&2 ; exit 1 ;;
  		:)
    			echo "Option -$OPTARG requires an argument." >&2 ; exit 1 ;;
		--) shift ; break ;;
		*)
			echo -e "${RED}Error: usage ./add_dns.sh -h <hostname>  -i <local-ip>${NC}">&2 ; exit 1 ;;
	esac
done

# Function check if patern exist in file
function 		patern_in_file()
{
	# Check 2 parameters provided
	if [ ${#1} -eq 0 ]
	then
		echo "No patern provided in partern_in_file()"
		exit 1;
	elif [ ${#2} -eq 0 ]
	then
		echo "No file provided in partern_in_file()"
		exit 1;
	fi

	# Check if file provided exist
	if [ ! -f "$2" ]
	then
		echo "Filename provided in patern_in_file() dind't exist"
		exit 1;
	fi

	exist=$(grep "$1" "$2")
	if [ ${#exist} -eq 0 ]
	then
		echo 0
		return 0
	else
		echo 1
		return 1
	fi
}

# Check if ip ping
echo -e "${YEL}ping_result=$(ping -c1 $ipv4 > /dev/null && echo true || echo false)${NC}"
ping_result=$(ping -c1 $ipv4 > /dev/null && echo true || echo false)

if [ $ping_result != true ]
then
	echo "IPv4 provided is not reachable"
	exit 1;
fi

# Check if a host is provided
if [ ${#hostname} -eq 0 ]
then
	echo "No host provide."
	exit 1;
fi

# Check if a domain is provided
if [ ${#domain_name} -eq 0 ]
then
	echo "No domain provide."
	exit 1;
fi

# Check if ipv4 or hostname already exist in file
ipv4_exist=$(patern_in_file "$ipv4" "/etc/hosts")
if [ $ipv4_exist -eq 1 ]
then
	echo -e "${RED}IPv4: '`echo $ipv4`' already exist in /etc/hosts${NC}"
	exit 1
fi
fqdn_exist=$(patern_in_file "${hostname}.${domain_name}" "/etc/hosts")
if [ $fqdn_exist -eq 1 ]
then
	echo -e "${RED}FQDN: '`echo ${hostname}.${domain_name}`' already exist in /etc/hosts${NC}"
	exit 1
fi

##
## Append new hostname entrie in /etc/hosts
##

# Create new line for /etc/hosts file, with ipv4, domain name and hostname
tab=$'\t'
echo -e "${YEL}new_line='"$ipv4  $hostname"'"${NC}
new_line_hosts="${ipv4}${tab}${hostname}.${domain_name}"

# add new line with hostname and ip before the first empty line
echo -e ${YEL}'sed -i.bak "/^$/ i '$new_line_hosts' /etc/hosts'${NC}
sed -i.bak "/^$/ i $new_line_hosts" /etc/hosts

# Check if newline has correctly be added
echo -e ${YEL}'line_number=$(grep "'$new_line_hosts'" /etc/hosts | wc -l)'${NC}
line_number_hosts=$(grep "$new_line_hosts" /etc/hosts | wc -l)

# If line_number if != 1 retore file previously backuped
if [ $line_number_hosts -ne 1 ]
then
	mv -v /etc/hosts.bak /etc/hosts
	echo -e "${RED}New line in /etc/hosts hasn't be added correctly. Abort${NC}"
	exit 1;
fi

##
## Append new DNS entrie in /etc/bind/db192
##

# Split IPv4 on '.' to get back last driplet digit
last_triplet=(${ipv4//./ })

# Create new line for /etc/bind/db.192 file
new_line_db192="${last_triplet[3]}${tab}IN${tab}PTR${tab}${hostname}"

# Check if /etc/bind/db.192 already contain hostname and domain name
db192_exist=$(patern_in_file "$new_line_db192" "/etc/bind/db.192")
if [ $db192_exist -eq 1 ]
then
	echo -e "${RED}New line: '`echo $new_line_db192`' already exist in /etc/bind/db.192${NC}"
	exit 1
fi

# add new line with domain and ip at end of file
echo -e ${YEL}'sed -i.bak "$ a '$new_line_db192'" /etc/bind/db.192'${NC}
sed -i.bak "$ a $new_line_db192" /etc/bind/db.192

# Check if newline has correctly be added
echo -e ${YEL}'line_number_db192=$(grep "'$new_line_db192'" /etc/bind/db.192 | wc -l)'${NC}
line_number_db192=$(grep "$new_line_db192" /etc/bind/db.192 | wc -l)

# If line_number_db192 if != 1 retore file previously backuped
if [ $line_number_db192 -ne 1 ]
then
	mv -v /etc/bind/db.192.bak /etc/bind/db.192
	echo -e "${RED}New line in /etc/bind/db.192 hasn't be added correctly. Abort${NC}"
	exit 1
fi

##
## Append new DNS entrie in /etc/bind/db.slash16.local
##

# Create new line for /etc/bind/db.slash16.local file
new_line_db_domain="${hostname}${tab}IN${tab}A${tab}${ipv4}"

# Check if /etc/bind/db.slash16.local already contain hostname and domain name
db192_exist=$(patern_in_file "$new_line_db_domain" "/etc/bind/db.slash16.local")
if [ $db192_exist -eq 1 ]
then
	echo -e "${RED}New line: '`echo $new_line_db_domain`' already exist in /etc/bind/db.slash16.local${NC}"
	exit 1
fi

# add new line with domain and ip at end of file
echo -e ${YEL}'sed -i.bak "$ a '$new_line_db_domain'" /etc/bind/db.slash16.local'${NC}
sed -i.bak "$ a $new_line_db_domain" /etc/bind/db.slash16.local

# Check if newline has correctly be added
echo -e ${YEL}'line_number_db_domain=$(grep "'$new_line_db_domain'" /etc/bind/db.slash16.local | wc -l)'${NC}
line_number_db_domain=$(grep "$new_line_db_domain" /etc/bind/db.slash16.local | wc -l)

# If line_number_db_domain if != 1 retore file previously backuped
if [ $line_number_db_domain -ne 1 ]
then
	mv -v /etc/bind/db.slash16.local.bak /etc/bind/db.slash16.local
	echo -e "${RED}New line in /etc/bind/db.slash16.local hasn't be added correctly. Abort${NC}"
	exit 1
fi

exit 0
