#!/bin/bash

library_path=".."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/system_functions.sh" ]
then
    # Include library
    . ${library_path}/shell_library/type_functions.sh
    . ${library_path}/shell_library/system_functions.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

DEBUG=1

# Trap CTRL C adn execute gracefulExit when catch it
trap gracefulExit INT

function	spinner()
{
	local pid=$!
	local delay=0.75
	local spinstr='...'

	while [ "$(ps a | awk '{print $1}' | grep $pid)" ]
	do
		local temp=${spinstr#?}
		printf "%s " "$spinstr"
		local spinstr=$temp${spinstr%"$temp"}
		sleep $delay
		printf "\b\b\b"
	done
	#printf "    \b\b\b\b"
	echo
}

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Ask user if he want to change root password
read -p 'Change root passwoord ? (y,n) ' CH_ROOT_PASSWD

while [ "$CH_ROOT_PASSWD" != 'y' ] && [ "$CH_ROOT_PASSWD" != 'n' ]
do
	read -p 'Change root passwoord ? (y,n) ' CH_ROOT_PASSWD	
done

# If user want to change root passwd, do it
if [ "$CH_ROOT_PASSWD" == 'y' ]
then
	# Change root password
	read -sp 'New root passwd: ' ROOT_PASSWD
	echo
	read -sp 'Configm root passwd: ' CONF_ROOT_PASSWD
	echo

	# Ask confirm root passwd while CONF_ROOT_PASSWD don't match with ROOT_PASSWD
	while [ ${ROOT_PASSWD} != ${CONF_ROOT_PASSWD} ]
	do
		echo "Password miss match, please try again"
		read -sp 'New root passwd: ' ROOT_PASSWD
		echo
		read -sp 'Configm root passwd: ' CONF_ROOT_PASSWD
		echo
	done
fi

# Ping public domain to check internet access
ping_result=$(ping -c1 'dns1.isp.ovh.net' > /dev/null && echo true || echo false)

if [ "$ping_result" != true ]
then
	echo "Can't ping outside world, abort"
	exit 1
fi

# Update repository
echo -e ${YEL}"apt-get update"${NC}
apt-get update > /dev/null &
spinner

# Install git and vim package 
echo -e ${YEL}"apt-get install sshpass git vim sudo"${NC}
apt-get install sshpass git vim sudo > /dev/null &
spinner

# Check if repo exist, if not clone it 
if [ ! -d "create_arch" ]
then
	echo -e ${YEL}"git clone https://github.com/bbichero/create_arch.git"${NC}
	git clone https://github.com/bbichero/create_arch.git > /dev/null &
	spinner
else
	echo -e ${RED}"create_arch directory already exist"${NC}
fi

# Check if repo exist, if not clone it 
if [ ! -d "shell_library" ]
then
	echo -e ${YEL}"git clone https://github.com/bbichero/shell_library"${NC}
	git clone https://github.com/bbichero/shell_library > /dev/null &
	spinner
else
	echo -e ${RED}"shell_library directory already exist"${NC}
fi

# Go
cd create_arch

# Check if argument file exist
if [ ! -f "arguments" ]
then
	echo -e ${RED}"argument file doesn't exist, if you want"
	echo -e "initialise a VM you must create it"${NC}
fi

# Check if config file exist
if [ ! -f "config" ]
then
	echo -e ${RED}"config file doesn't exist"
	echo -e "This file permit to inform ./ft_install_arch that"
	echo -e "DHCP and DNS machine exist for add entries to them"${NC}
fi

# Execute ft_ini_gateway.sh file to allow ip forwarding
# and create /etc/network/ip-pre-up.d/iptables
echo -e ${YEL}"Initialise gateway"${NC}
./ft_ini_gateway.sh

echo "No you can run :"
echo "./ft_install_arch.sh <argument-file>"
exit 1 
