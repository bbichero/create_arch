#!/bin/bash
# Take one parameter, a file with all arguments needed by this script
# TODO: remove root login in ssh in /etc/ssh/sshd_config file
# TODO: In other srcipt check DEBUG variable existance to rewirte or not it

library_path=".."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/variable_functions.sh" ] &&
   	[ -f "${library_path}/shell_library/edit_functions.sh" ] &&
   	[ -f "${library_path}/file_parsing/array_check.sh" ] &&
   	[ -f "${library_path}/file_parsing/array_extract.sh" ]
then
    # Include library
    . ${library_path}/shell_library/variable_functions.sh
    . ${library_path}/shell_library/type_functions.sh
    . ${library_path}/shell_library/edit_functions.sh
    . ${library_path}/file_parsing/array_check.sh
    . ${library_path}/file_parsing/array_extract.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Trap CTRL C adn execute gracefulExit when catch it
trap gracefulExit INT

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

DEBUG=2
new_file="arguments.clean"
log_file="create_arch.log"

# Declare all valid keywork
declare -a keywords
keywords=("root-passwd" "domain-name" "new-root-passwd" "ip" "dhcp-ip" "hostname" "bashrc" "vimrc")
keywords_array=("username" "port-forward")
keywords_mand=("domain-name" "root-passwd" "new-root-passwd" "ip" "dhcp-ip" "hostname" "username")
keywords_conf=("gate" "dns" "dhcp")

# Check if argument
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if file given exist
if [ ! -f "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

# Check config file and store information in array
function	check_config_file()
{
	# if $DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in check_config_file() with array '`echo ${1}`'"${NC}
	fi

	# Redeclare array localy
	local clean_array=("${@:1}")

	for i in ${!clean_array[@]}
	do
		# Split line with ':' delimitor
		IFS=':' read -r -a split_line <<< "${clean_array[$i]}"
		split_line[0]=$(echo "${split_line[0]}" | xargs)
		split_line[1]=$(echo "${split_line[1]}" | xargs)

		# if $DEBUG>1 print info
		if [ $DEBUG -gt 1 ]
		then
			echo -e ${YEL}"Lines split with ':' delimitor"
			echo "Line one : "${split_line[0]}
			echo -e "Line two: "${split_line[1]}${NC}
		fi

		# Check if split_line[1] is a valid ipv4
		if ! ft_is_ipv4 "${split_line[1]}"
		then
			exit 1
		fi

		# Check if array keywords_conf contain $split_line[0]
		if ft_array_contains "${split_line[0]}" "${keywords_conf[@]}"
		then
			echo -e ${RED}"Keyword '`echo ${split_line[0]}`' is invalid, please edit you config file\nAbort."${NC}
			exit 1
		fi

		# Put value in config_array with key
		config_array["${split_line[0]}"]="${split_line[1]}"

		if [ $DEBUG -gt 1 ]
		then
			echo "config array: key: ${split_line[0]}"
			echo "value: "${config_array[${split_line[0]}]}
		fi
	done

	unset clean_array split_line
}

# Remove duplicate and create array to compare the 2 data contents
no_duplicate=$(cat "$1" | sort -u | sed '/^\s*$/d')
readarray -t no_dupli_arr <<<"$no_duplicate"
file_content=$(cat "$1" | sort | sed '/^\s*$/d')
readarray -t file_arr <<<"$file_content"

# Return diff between 2 arrays
array_diff=$(printf '%s\n' "${no_dupli_arr[@]}" "${file_arr[@]}" | sort | uniq -u)

# If duplicate, create new file with clean lines
if [ ${#array_diff} -eq 0 ]
then
	echo -e ${RED}"Duplicate find in file, create fresh new file"${NC}
	echo "$no_duplicate" > "$1.no"
	# Put all line in array, and trim
	readarray file_content_arr < "$1".no
else
	readarray file_content_arr < "$1"
fi

# Unset unused variables
unset no_duplicate file_content

declare -a file_content

# Delele and recreate new file for push clean line into
if [ -f ${new_file} ]
then
	rm ${new_file}
fi
touch ${new_file}

# Parse file given in parameter, line by line
for i in "${!file_content_arr[@]}"
do
	check_next_line "${file_content_arr[$i]}"
done

# If $keywords_mand array contain at least one value
# Not all mandatory keywords have been provide so abort
if [ "${#keywords_mand[@]}" != 0 ]
then
	echo -e ${RED}"Not all mandatory keywords have been provided, missing:"
	echo -e ${keywords_mand[@]}${NC}
	exit 1
fi

# if check_next_line has never exit script due to bad line format in argv file,
# We get clean array from new file created
readarray clean_array < ${new_file}

# Now we execute each sript coresponding to keyword
# First we execute every mandatory keyword
# We fisrt check if keyword is valid
keyword='ip'
if ft_valid_keyword "${keyword}"
then
	ip_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Then we get ip-dhcp
keyword='dhcp-ip'
if ft_valid_keyword "${keyword}"
then
	dhcp_ip_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# And we get hostname
keyword='hostname'
if ft_valid_keyword "${keyword}"
then
	hostname_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# And we get domain-name
keyword='domain-name'
if ft_valid_keyword "${keyword}"
then
	domain_name_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Get back root-password value
keyword='root-passwd'
if ft_valid_keyword "${keyword}"
then
	root_passwd_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Get back new-root-password value
keyword='new-root-passwd'
if ft_valid_keyword "${keyword}"
then
	new_root_passwd_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Get back usernames value
keyword='username'
if ft_valid_keyword "${keyword}"
then
	username_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

# Get back vimrc value
keyword='vimrc'
if ft_valid_keyword "${keyword}"
then
	vimrc_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Get back bashrc value
keyword='bashrc'
if ft_valid_keyword "${keyword}"
then
	bashrc_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Get back root-password value
keyword='port-forward'
if ft_valid_keyword "${keyword}"
then
	port_forward_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

# If DEBUG>0 show all key and values getting from file
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Show all val from argument file"
	echo "ip: '${ip_val}'"
	echo "dhcp-ip: '${dhcp_ip_val}'"
	echo "hostname: '${hostname_val}'"
	echo "domain-name: '${domain_name_val}'"
	echo "root-passwd: '${root_passwd_val}'"
	echo "new-root-passwd: '${new_root_passwd_val}'"
	echo "username: '${username_val[@]}'"
	if [ ${#vimrc_val} -ne 0 ]
	then
		echo "vimrc: '${vimrc_val}'"
	fi
	if [ ${#bashrc_val} -ne 0 ]
	then
		echo "bashrc: '${bashrc_val}'"
	fi
	if [ ${#port_forward_val[@]} -ne 0 ]
	then
		echo -e "port-forward: '${port_forward_val[@]}'"${NC}
	fi
fi

# ssh connection, redirect stderr to stdout to catch message in variable
echo -e ${YEL}"sshpass -p '$root_passwd_val' ssh root@${ip_val} 'exit' 2>&1"${NC}
ssh_return="$(sshpass -p "$root_passwd_val" ssh root@${ip_val} 'exit' 2>&1)"

# if ssh_return isn't empty check if no key that must be change or whatever
if [ ${#ssh_return} -ne 0 ]
then
	echo -e ${RED}"Can't connect to ip '$ip_val'"${NC}

	# If DEBUG>0 print info
	if [ $DEBUG -gt 0 ]
	then
		echo "ssh return: "
		echo "$ssh_return"
	fi

	# IF error message is "WARNING REMOTE HOST IDENTIFICATION ..."
	# Remove ip from know host
	if [ "$(echo "${ssh_return}" | grep 'REMOTE HOST IDENTIFICATION HAS CHANGED')" != "" ]
	then
		#echo -e ${YEL}ssh-keygen -R "$ip_val"${NC}
		echo -e ${YEL} ssh-keygen -f "/home/bbichero/.ssh/known_hosts" -R $ip_val${NC}
		ssh-keygen -f "/home/bbichero/.ssh/known_hosts" -R ${ip_val}
		echo -e ${YEL}ssh-keygen -f "/root/.ssh/known_hosts" -R $ip_val${NC}
		ssh-keygen -f "/root/.ssh/known_hosts" -R ${ip_val}
	else
		echo -e ${RED}"Can't connect to remote VM abort."${NC}
		exit 1
	fi
fi

# Check if file ssh_rsa/ssh_rsa_connection.sh exist
if [ ! -f "./ssh_rsa/ssh_rsa_connection.sh"  ]
then
	echo -e ${RED}"ssh_rsa_connection.sh can't be find, abort."${NC}
	exit 1
fi

# If root-password and new-root-password have same value do nothing
if [ "$root_passwd_val" != "$new_root_passwd_val" ]
then
	# Now connect to ip and change root password
	echo "sudo sshpass -p $root_passwd_val ssh -o PasswordAuthentication=no root@${ip_val} echo -e '${new_root_passwd_val}\n${new_root_passwd_val}' | passwd 2>&1"
	chg_passwd_result=$(sudo sshpass -p "$root_passwd_val" ssh -o PasswordAuthentication=no root@${ip_val} 'echo -e "${new_root_passwd_val}\n${new_root_passwd_val}" | passwd 2>&1')

	# Check last command result
	if [ "$(echo "$chg_passwd_result" | grep 'password updated successfully')" == "" ]
	then
		new_root_passwd_val="$root_passwd_val"
		echo -e ${RED}"Root password hasn't be change"${NC}
	else
		echo -e ${GRE}"Root password changed succefully"${NC}
	fi
else
	echo -e ${YEL}"Root passwords in argument file are the same"${NC}
fi

# Check if can connect with rsa key
echo -e ${YEL}"ssh -o PasswordAuthentication=no root@${ip_val} exit &>/dev/null"${NC}
ssh -o PasswordAuthentication=no root@${ip_val} exit &>/dev/null
if [ "$(test $? == 0 && echo 'true' || echo 'false')" == "false" ]
then
	# Create ssh key to connect to vm whitout password
	echo "Create ssh rsa key and pass it to host '`echo $ip_val`'" > ${log_file}
	echo "./ssh_rsa/ssh_rsa_connection.sh -u root -i ${ip_val} -s '$new_root_passwd_val'" >> ${log_file}
	./ssh_rsa/ssh_rsa_connection.sh -u root -i ${ip_val} -s "$new_root_passwd_val"

	# If ssh__rsa_connection.sh file exit with 1, abort
	if [ "$(test $? == 0 && echo 'true' || echo 'false')" == "false" ]
	then
		echo -e ${RED}"ssh_rsa_connection.sh file abort with exit code 1"${NC}
		exit 1
	fi
fi

# if DEBUG>1 print info
if [ $DEBUG -gt 1 ]
then
	echo -e ${YEL}"Loop on 'username_val' array and create users"${NC}
fi

# Add return line to log file
echo >> ${log_file}

# Loop username_val, like it's an array (perhabs it's not but don't care)
for i in "${!username_val[@]}"
do
	# Get in variable username and password from argument file
	username_toadd=$(echo "${username_val[$i]}" | sed 's/{//;s/}//')

	# Split line with ',' delimitor
	IFS=',' read -r -a username_toadd <<< "${username_toadd}"

	# if DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Will create '${username_toadd[0]}' with passwd: '${username_toadd[1]}'"${NC}
	fi

	# Add user in new vm
	adduser_return=$(ssh root@${ip_val} "echo -e '${username_toadd[1]}\n${username_toadd[1]}' | adduser ${username_toadd[0]}" 2>&1)

	# Add sudo rigth for first username find in argument file
	if [ "$i" -eq 0 ]
	then
		# if DEBUG>1 print info
		if [ $DEBUG -gt 1 ]
		then
			echo -e ${YEL}"Will add '${username_toadd[0]}' to sudo, root and adm group"${NC}
		fi

		adduser_sudo=$(ssh root@${ip_val} "usermod -G sudo,adm,root ${username_toadd[0]}" 2>&1)
		echo "$adduser_sudo" >> ${log_file}
	fi

	# Check if previous command adduser contain 'user `username` already exist'
	if [ "$(echo "$adduser_return" | grep 'already exists.')" != "" ]
	then
		echo -e ${RED}"User '`echo ${username_toadd[0]}`' already exist on host '`echo $ip_val`'"${NC}
	fi
	# Send info of previous command to log_file
	echo "${adduser_return}" >> ${log_file}
done

# Add return line to log file
echo >> ${log_file}

# Get mac addr of new vm
mac_addr=$(ssh root@${ip_val} "ifconfig eth1 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'")
echo "Mac address of new vm: '`echo $mac_addr`'" >> ${log_file}

# If mac_addr varibale return null, abort, cant process followgin command
if [ "$mac_addr" == "" ]
then
	echo -e ${RED}"Can't optain MAC address of host, abort"${NC}
	exit 1
fi

# Change hosts and remove unused line in /etc/hosts
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Edit /etc/hosts file and add new hostname '`echo $hostname_val`'"${NC}
fi
# Add info to log file
echo "ssh root@${ip_val} sed -i.bak 's/127.0.0.1\tlocalhost/127.0.0.1\t${hostname_val}\t${hostname_val}.${domain_name_val}/g;/127.0.1.1\tSource/d' /etc/hosts" >> ${log_file}
ssh root@${ip_val} "sed -i.bak 's/127.0.0.1\tlocalhost/127.0.0.1\t${hostname_val}\t${hostname_val}.${domain_name_val}/g;/127.0.1.1\tSource/d' /etc/hosts"

# Change hostname in /etc/hostname
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Edit /etc/hostname file and add new hostname '`echo $hostname_val`'"${NC}
fi

# Add info to log file
echo "ssh root@${ip_val} sed -i.bak '1 s/^.*$/${hostname_val}/' /etc/hostname" >> ${log_file}
ssh root@${ip_val} "sed -i.bak '1 s/^.*$/${hostname_val}/' /etc/hostname"

# Check if bashrc file exist
if [ ${#bashrc_val} -ne 0 ]
then
	# Check if file exist
	if [ ! -f "${bashrc_val}" ]
	then
		echo -e ${RED}"File: '${bashrc_val}' not found, abort"${NC}
		exit 1
	fi

	if [ $DEBUG -gt 0 ]
	then
		echo -e ${YEL}"Copy bashrc file to new vm for every users"${NC}
	fi

	# Add bashrc to remote /root directory only if he didn't exist already
	if ! ssh root@${ip_val} stat /root/.bashrc \> /dev/null 2\>\&1
	then
		echo "scp ${bashrc_val} root@${ip_val}:/root/.bashrc" >> ${log_file}
		scp ${bashrc_val} root@${ip_val}:/root/.bashrc
	fi

	# Add .bashrc file to every user directory in /home
	echo "ssh root@${ip_val} for d in /home/*; do if [ -d "$d" ] ; then cp /root/.bashrc "$d"; fi done" >> ${log_file}
	ssh root@${ip_val} "for d in /home/*; do if [ -d "$d" ] ; then cp /root/.bashrc "$d"; fi done"
fi

# Check if vimrc file exist
if [ ${#vimrc_val} -ne 0 ]
then
	# Check if file exist
	if [ ! -f "${vimrc_val}" ]
	then
		echo -e ${RED}"File: '${vimrc_val}' not found, abort"${NC}
		exit 1
	fi
	
	if [ $DEBUG -gt 0 ]
	then
		echo -e ${YEL}"Copy vimrc file to new vm for every users"${NC}
	fi

	# Add vimrc to remote /root directory only if he didn't exist already
	if ! ssh root@${ip_val} stat /root/.vimrc \> /dev/null 2\>\&1
	then
		echo "scp ${vimrc_val} root@${ip_val}:/root/.vimrc" >> ${log_file}
		scp ${vimrc_val} root@${ip_val}:/root/.vimrc
	fi

	# Add .vimrc file to every user directory in /home
	echo "ssh root@${ip_val} for d in /home/*; do if [ -d "$d" ] ; then cp /root/.vimrc "$d"; fi done" >> ${log_file}
	ssh root@${ip_val} "for d in /home/*; do if [ -d "$d" ] ; then cp /root/.vimrc "$d"; fi done"
fi

# Check existance of config file
if [ -f "config" ]
then
	# Put all config content in array and declare new array to stock new config data
	readarray config_content < "config"
	declare -A config_array

	# Check validity of config file
	check_config_file "${config_content[@]}"
else
	echo -e ${RED}"config file are not in current directory, abort"${NC}
	exit 1
fi

# Forward ports (DHCP must be set before)
for i in "${!port_forward_val[@]}"
do
	# Get in variable port from argument file
	port_to_forward=$(echo "${port_forward_val[$i]}" | sed 's/{//;s/}//')

	# Split line with ',' delimitor
	IFS=',' read -r -a port_split <<< "${port_to_forward}"

	# Check if port not already in use in current host
	echo -e ${YEL}"nc -z 127.0.0.1 ${port_split[1]}"'; echo $?'${NC}

	if [ "$(sudo nc -z 127.0.0.1 ${port_split[1]}'; echo $?')" == '1' ]
	then
		# Check if port not already in use in remote host
		echo -e ${YEL}"ssh root@${ip_val} "nc -z 127.0.0.1 ${port_split[0]}'; echo $?'${NC}

		if [ "$(sudo ssh root@${ip_val} nc -z 127.0.0.1 ${port_split[0]}'; echo $?')" == '1' ]
		then
			echo "./ft_forward_ports.sh -s '${port_split[0]}' -d '${port_split[1]}' -i '${dhcp_ip_val}' -f 'yes'"
			./ft_forward_ports.sh -s "${port_split[0]}" -d "${port_split[1]}" -i "${dhcp_ip_val}"
		else
			port_use=$(lsof -i :${port_split[0]} -S | sed '1d' | cut -d '' -f 1)
			echo -e ${RED}"Port '${port_split[0]}' already in use by : '$port_use'"${NC}
		fi
	else
		port_use=$(lsof -i :${port_split[1]} -S | sed '1d' | cut -d '' -f 1)
		echo -e ${RED}"Port '${port_split[1]}' already in use by '$port_use'"${NC}
	fi
done

# Check if ft_add_to_dns.sh file exist on DNS machine
add_dns_file=$(ssh root@${config_array["dns"]} "[ -f 'ft_add_to_dns.sh' ]")
add_dns_file=$(ssh root@${config_array["dns"]} 'echo $?')
if [ "${add_dns_file}" != 0 ]
then
	echo -e ${RED}"File ft_add_to_dns.sh not find in remote VM, abort"${NC}
	exit 1
else
	# Execute ft_add_to_dns.sh file on dns VM
	echo "ssh root@${config_array["dns"]} './ft_add_to_dns.sh -h ${hostname_val} -i ${dhcp_ip_val} -d ${domain_name_val}'"
	ssh root@${config_array["dns"]} "./ft_add_to_dns.sh -h ${hostname_val} -i ${dhcp_ip_val} -d ${domain_name_val}"

	# Check if file previously execute, has not return exit code 1
	file_exit=$(ssh root@${config_array["dns"]} 'echo $?')
	if [ "${file_exit}" != 0 ]
	then
		echo -e ${RED}"File ft_add_to_dns.sh exit with code 1, abort"${NC}
	#	exit 1
	fi
fi

# execute ./ft_add_to_dhcp script on dns machine 
# Add DHCP entry to DHCP vm
echo "./ft_add_to_dhcp.sh -s '${config_array["dhcp"]}' -m '$mac_addr' -i '$dhcp_ip_val' -h '$hostname_val' -d '$domain_name_val'"
./ft_add_to_dhcp.sh -s "${config_array["dhcp"]}" -m "$mac_addr" -i "$dhcp_ip_val" -h "$hostname_val" -d "$domain_name_val"

# Restart eht1 network interface of remote VM
echo -e ${GRE}ssh root@${ip_val} "ifdown eth1 ; ifup eth1 2>&1"${NC}
ssh root@${ip_val} "ifdown eth1 ; ifup eth1 2>&1"

# Check last exit code
if [ $? -ne 0 ]
then
	echo "ft_add_to_dhcp script exit with '`echo $?`', abort" >> ${log_file}
	#exit 1
fi

# Update repository package
echo "ssh root@${ip_val} 'apt-get update && apt-get -y install vim sudo && apt-get -y upgrade'" 
ssh root@${ip_val} "apt-get update && apt-get -y install vim sudo && apt-get -y upgrade" 

# Reboot VM
echo "ssh root@${ip_val} 'reboot'"
ssh root@${ip_val} "reboot"
