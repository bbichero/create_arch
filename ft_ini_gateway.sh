#!/bin/bash

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

PATH=/usr/sbin:/sbin:/bin:/usr/bin

#
# delete all existing rules.
#
./fw.stop

# Always accept loopback traffic
iptables -A INPUT -i lo -j ACCEPT

# Allow outgoing connections from the LAN side.
iptables -A FORWARD -i eth1 -j ACCEPT
iptables -A FORWARD -o eth1 -j ACCEPT

# Masquerade
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

# Don't forward from the outside to the inside.
iptables -A FORWARD -i eth0 -o eth0 -j REJECT

# Enable routing.
#echo 1 > /proc/sys/net/ipv4/ip_forward
sed -i.bak 's/^#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf

# Save iptables rules
iptables-save > /etc/iptables.up.rules

# Create file that will be launch every vm start
echo '#!/bin/bash
# Script must be run as root, so check EUID in user s env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

/sbin/iptables-save > /etc/iptables.up.rules
/sbin/iptables-restore < /etc/iptables.up.rules' > /etc/network/if-pre-up.d/iptables

# Change file permission (add execution)
chmod +x /etc/network/if-pre-up.d/iptables
